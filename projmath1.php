<?php

function h($a, $b, $n)
{
    return ((($b - $a)) / $n);
}

function rectangles($a, $b, $n, $pfunction)
{
    $cumulate = 0;
    $h = h($a, $b, $n);
    for ($i = 0; $i < $n; $i++)
        $cumulate += $pfunction($a + ($h * $i));
    return ($h * $cumulate);
}

function create_sd_table()
{
    $hgc = html_entity_decode('&#9556;', ENT_NOQUOTES, 'UTF-8');
    $hdc = html_entity_decode('&#9559;', ENT_NOQUOTES, 'UTF-8');
    $bgc = html_entity_decode('&#x255a;', ENT_NOQUOTES, 'UTF-8');
    $bdc = html_entity_decode('&#x255d;', ENT_NOQUOTES, 'UTF-8');
    $linev = html_entity_decode('&#9553;', ENT_NOQUOTES, 'UTF-8');
    $lineh = html_entity_decode('&#9552;', ENT_NOQUOTES, 'UTF-8');
    $ig = html_entity_decode('&#9568;', ENT_NOQUOTES, 'UTF-8');
    $id = html_entity_decode('&#9571;', ENT_NOQUOTES, 'UTF-8');
    $ih = html_entity_decode('&#9574;', ENT_NOQUOTES, 'UTF-8');
    $ib = html_entity_decode('&#9577;', ENT_NOQUOTES, 'UTF-8');
    $im = html_entity_decode('&#9580;', ENT_NOQUOTES, 'UTF-8');
    $a_density1 = 0;
    $b_density1 = 100;
    $n_density1 = 2000;
    $a_density2 = 1;
    $b_density2 = 100;
    $n_density2 = 1980;
    $a_density3 = 0;
    $b_density3 = 100;
    $n_density3 = 2000;
    $a_density4 = -8;
    $b_density4 = 10;
    $n_density4 = 360;
    $es1 = simpson($a_density1, $b_density1, $n_density1, 'd1');
    $es2 = simpson($a_density2, $b_density2, $n_density2, 'd2');
    $es3 = simpson($a_density3, $b_density3, $n_density3, 'd3');
    $es4 = simpson($a_density4, $b_density4, $n_density4, 'd4');
    $var1 = simpson_for_var($a_density1, $b_density1, $n_density1, 'variance_density_one', $es1);
    $var2 = simpson_for_var($a_density2, $b_density2, $n_density2, 'variance_density_two', $es2);
    $var3 = simpson_for_var($a_density3, $b_density3, $n_density3, 'variance_density_three', $es3);
    $var4 = simpson_for_var($a_density4, $b_density4, $n_density4, 'variance_density_four', $es4);
    echo $hgc . str_repeat($lineh, 24)  . $ih . str_repeat($lineh, 17) . $ih . str_repeat($lineh, 17) . $hdc . "\n" .
        $linev . ' Densité de probabilité ' . $linev . '       E(X)      ' . $linev . '       V(X)      ' . $linev . "\n" .
        $ig . str_repeat($lineh, 24) .  $im . str_repeat($lineh, 17) . $im . str_repeat($lineh, 17) . $id . "\n" .
        $linev . '     d1(x) [0, 100]     ' . $linev . $es1 . str_repeat(' ', 17 - strlen($es1)) . $linev . $var1 . str_repeat(' ', 17 - strlen($var1)) . $linev . "\n" .
        $linev . '     d2(x) [1, 100]     ' . $linev . $es2 . str_repeat(' ', 17 - strlen($es2)) . $linev . $var2 . str_repeat(' ', 17 - strlen($var2)) . $linev . "\n" .
        $linev . '     d3(x) [0, 100]     ' . $linev . $es3 . str_repeat(' ', 17 - strlen($es3)) . $linev . $var3 . str_repeat(' ', 17 - strlen($var3)) . $linev . "\n" .
        $linev . '     d4(x) [-8, 100]    ' .  $linev . $es4 . str_repeat(' ', 17 - strlen($es4)) . $linev . $var4 . str_repeat(' ', 17 - strlen($var4)) . $linev . "\n" .
        $bgc . str_repeat($lineh, 24)  . $ib . str_repeat($lineh, 17) . $ib . str_repeat($lineh, 17) . $bdc . "\n";
}

function create_ft_table()
{
    $hgc = html_entity_decode('&#9556;', ENT_NOQUOTES, 'UTF-8');
    $hdc = html_entity_decode('&#9559;', ENT_NOQUOTES, 'UTF-8');
    $bgc = html_entity_decode('&#x255a;', ENT_NOQUOTES, 'UTF-8');
    $bdc = html_entity_decode('&#x255d;', ENT_NOQUOTES, 'UTF-8');
    $linev = html_entity_decode('&#9553;', ENT_NOQUOTES, 'UTF-8');
    $lineh = html_entity_decode('&#9552;', ENT_NOQUOTES, 'UTF-8');
    $ig = html_entity_decode('&#9568;', ENT_NOQUOTES, 'UTF-8');
    $id = html_entity_decode('&#9571;', ENT_NOQUOTES, 'UTF-8');
    $ih = html_entity_decode('&#9574;', ENT_NOQUOTES, 'UTF-8');
    $ib = html_entity_decode('&#9577;', ENT_NOQUOTES, 'UTF-8');
    $im = html_entity_decode('&#9580;', ENT_NOQUOTES, 'UTF-8');
    $hf1 = "∫ƒ1(x)dx";
    $hf2 = "∫ƒ2(x)dx";
    $hf3 = "∫ƒ3(x)dx";
    $hf4 = "∫ƒ4(x)dx";
    $r1 = rectangles(1, 2, 20, 'f1');
    $r2 = rectangles(1, 2, 20, 'f2');
    $r3 = rectangles(1, 2, 20, 'f3');
    $r4 = rectangles(1, 2, 20, 'f4');
    $tr1 = trapezoids(1, 2, 20, 'f1');
    $tr2 = trapezoids(1, 2, 20, 'f2');
    $tr3 = trapezoids(1, 2, 20, 'f3');
    $tr4 = trapezoids(1, 2, 20, 'f4');
    $s1 = simpson(1, 2, 20, 'f1');
    $s2 = simpson(1, 2, 20, 'f2');
    $s3 = simpson(1, 2, 20, 'f3');
    $s4 = simpson(1, 2, 20, 'f4');
    echo $hgc . str_repeat($lineh, 48)  . $ih . str_repeat($lineh, 16) . $ih . str_repeat($lineh, 17);
    echo $ih . str_repeat($lineh, 16) . $ih . str_repeat($lineh, 17) . $hdc . "\n";
    echo $linev  . str_repeat(' ', 48) . $linev . $hf1 . str_repeat(' ', 8) . $linev . $hf2 . str_repeat(' ', 9);
    echo $linev . $hf3 . str_repeat(' ', 8) . $linev . $hf4 . str_repeat(' ', 9) . $linev . "\n";
    echo  $ig . str_repeat($lineh, 48) . $im . str_repeat($lineh, 16) . $im . str_repeat($lineh, 17) . $im . str_repeat($lineh, 16);
    echo $im . str_repeat($lineh, 17) . $id . "\n";
    echo $linev . ' valeur obtenue en phase 1 arrondie au centième ' . $linev . round(primitive(1, 2, 'F_1'), 2).'            ' ;
    echo $linev . round(primitive(1, 2, 'F_2'), 2).'             ';
    echo $linev . round(primitive(1, 2, 'F_3'), 2).'            ' . $linev.round(primitive(1, 2, 'F_4'), 2).'             '.$linev ."\n" ;
    echo $linev . '              méthode du rectangle              ' . $linev . $r1 . str_repeat(' ', 16 - strlen($r1)) . $linev . $r2 . str_repeat(' ', 17 - strlen($r2)) . $linev . $r3 . str_repeat(' ', 16 - strlen($r3)) . $linev . $r4 . str_repeat(' ', 17 - strlen($r4)) . $linev . "\n" .
    $linev . '              méthode des trapèzes              ' . $linev . $tr1 . str_repeat(' ', 16 - strlen($tr1)) . $linev . $tr2 . str_repeat(' ', 17 - strlen($tr2)) . $linev . $tr3 . str_repeat(' ', 16 - strlen($tr3)) . $linev . $tr4 . str_repeat(' ', 17 - strlen($tr4)) . $linev . "\n" .
    $linev . '              méthode de Simpson                ' . $linev . $s1 . str_repeat(' ', 16 - strlen($s1)) . $linev . $s2 . str_repeat(' ', 17 - strlen($s2)) . $linev . $s3 . str_repeat(' ', 16 - strlen($s3)) . $linev . $s4 . str_repeat(' ', 17 - strlen($s4)) . $linev . "\n" .
    $bgc . str_repeat($lineh, 48)  . $ib . str_repeat($lineh, 16) . $ib . str_repeat($lineh, 17) . $ib . str_repeat($lineh, 16) . $ib . str_repeat($lineh, 17) . $bdc . "\n";
    echo "La méthode de Simpson est la celle qui donne le meilleur résultat\n";
}

function f1($x)
{
    $fx = pow($x, 2);
    return ($fx);
}

function f2($x)
{
    $fx = 1 / (1 + $x);
    return $fx;
}
function f3($x)
{
    $fx = exp($x);
    return ($fx);
}
function f4($x)
{
    $fx = sin($x) ;
    return ($fx);
}

function trapezoids($a, $b, $n, $pfunction)
{
    $amout = 0;
    $h = h($a, $b, $n);
    $fx_part1 = (($pfunction($a) + $pfunction($b)) / 2);
    for ($i = 1; $i < $n; $i++)
        $amout += $pfunction($a + ($h * $i));
    $fx = $h * ($fx_part1 + $amout);
    return ($fx);
}

function simpson($a, $b, $n, $pfunction)
{
    $amout1 = 0;
    $amout2 = 0;
    $h = h($a, $b, $n);
    $fx_part1 = ($pfunction($a) + $pfunction($b));
    for ($i = 1; $i < $n; $i++)
        $amout1 += $pfunction($a + ($h * $i));
    for ($k = 0; $k < $n; $k++)
        $amout2 += $pfunction(((($a + ($h * $k)) + ($h / 2))));
    $fx = $h * (($fx_part1 + (2 * $amout1) + (4 * $amout2))) / 6;
    return ($fx);
}

function F_1($x)
{
    return ((1 / 3) * pow($x, 3));
}

function F_2($x)
{
    return (log(1 + $x));
}

function F_3($x)
{
    return (exp($x));
}

function F_4($x)
{
    return (-cos($x));
}

function primitive($a, $b, $pfunction)
{
    return ($pfunction($b) - $pfunction($a));
}

function d1($xi)
{
    $res = $xi * (4 * $xi * exp(-2 * $xi));
    return $res;
}

function d2($xi)
{
    $res = $xi * (3 / (pow($xi, 4)));
    return $res;
}

function d3($xi)
{
    $res = $xi * (2 * $xi * exp(-1 * (pow($xi, 2))));
    return $res;
}

function d4($xi)
{
    $res = $xi * ((1 / (sqrt(2 * M_PI))) * exp(-(1 / 2) * pow($xi-2, 2)));
    return $res;
}


function variance_density_one($xi, $es)
{
    $res = pow(($xi - $es), 2) * (4 * $xi * exp(-2 * $xi));
    return $res;
}

function variance_density_two($xi, $es)
{
    $res = pow(($xi - $es), 2) * (3 / (pow($xi, 4)));
    return ($res);
}

function variance_density_three($xi, $es)
{
    $res = pow(($xi - $es), 2) * (2 * $xi * exp(-1 * (pow($xi, 2))));
    return ($res);
}

function variance_density_four($xi, $es)
{
    $res = pow(($xi - $es), 2) * ((1 / (sqrt(2 * M_PI)))
            * exp(-(1 / 2) * pow($xi -2, 2)));
    return ($res);
}

function simpson_for_var($a, $b, $n, $pfunction, $es)
{
    $amount1 = 0;
    $amount2 = 0;
    $h = h($a, $b, $n);
    for($i = 1; $i < $n; $i++)
        $amount1 += $pfunction($a + $h * $i, $es);
    for($j = 0; $j < $n; $j++)
        $amount2 += $pfunction(($a + $j * $h) + ($h / 2), $es);
    $amount = $h * ($pfunction($a, $es) + $pfunction($b, $es) + 2
            * $amount1 + 4 * $amount2) / 6;
    return ($amount);
}

create_ft_table();
create_sd_table();



